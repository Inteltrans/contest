<?php

namespace common\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "requests".
 *
 * @property integer $requestkey
 * @property string $worklink
 * @property integer $contestkey
 * @property integer $organizationkey
 * @property integer $user_owner_key
 * @property string $teacher
 * @property string $user
 *
 * @property ContestUsers $teacherkey0
 * @property ContestUsers $id0
 * @property Contests $contestkey0
 * @property Results[] $results
 */
class Requests extends \yii\db\ActiveRecord
{
	
	public $file;
	
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'requests';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

//            [['contestkey', 'userkey'], 'required'],
//            [['contestkey', 'userkey', 'teacherkey'], 'integer'],
//            [['contestkey', 'userkey'], 'required', 'message' => 'Поле {attribute} не должно быть пустым'],
//            [['contestkey', 'userkey', 'teacherkey'], 'integer'],
            [['worklink','user', 'teacher'], 'string', 'max' => 100,'message' => 'Максимальный размер для поля {attribute} 100 символов'],
            [['file'], 'file','extensions' => 'png, jpg, doc, docx, ppt, pptx, pdf, mp4, mp3','message' => 'Загрузите работу','wrongExtension'=>'Ошибка формата.Разрешенные форматы: png, jpg, doc, docx, ppt, pptx, pdf, mp4, mp3','uploadRequired'=>'Файл не загружен'],
            //[['teacherkey'], 'exist', 'skipOnError' => true, 'targetClass' => ContestUsers::className(), 'targetAttribute' => ['teacherkey' => 'id']],
//            [['userkey'], 'exist', 'skipOnError' => true, 'targetClass' => ContestUsers::className(), 'targetAttribute' => ['userkey' => 'id']],
            [['user_owner_key'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_owner_key' => 'id']],
            [['organizationkey'], 'exist', 'skipOnError' => true, 'targetClass' => Organizations::className(), 'targetAttribute' => ['organizationkey' => 'organizationkey']],
            [['contestkey'], 'exist', 'skipOnError' => true, 'targetClass' => Contests::className(), 'targetAttribute' => ['contestkey' => 'contestkey']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'requestkey' => 'Requestkey',
            'worklink' => 'Ссылка на работу',
            'contestkey' => 'Конкурс',
            'user' => 'Участник ФИО',
            'teacher' => 'Учитель ФИО',
            'file'=>'Файл работы',
            'organizationkey' => 'Организация'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getid0()
    {
        return $this->hasOne(ContestUsers::className(), ['id'=>'userkey']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContestkey0()
    {
        return $this->hasOne(Contests::className(), ['contestkey' => 'contestkey']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResults()
    {
        return $this->hasMany(Results::className(), ['requestkey' => 'requestkey']);
    }
    
    public function beforeValidate() {
        if($this->isNewRecord){
                $this->file = UploadedFile::getInstance($this, 'file');
        }
       return parent::beforeValidate();
    }
}
