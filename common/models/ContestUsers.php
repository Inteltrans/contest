<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "contest_users".
 *
 * @property integer $id
 * @property string $firstname
 * @property string $secondname
 * @property string $lastname
 * @property string $birthday
 * @property string $username
 * @property string $password
 * @property integer $role
 * @property string $phonenumber
 * @property string $email
 * @property integer $organizationkey
 *
 * @property Organizations $organizationkey0
 * @property Requests[] $requests
 * @property Requests[] $requests0
 */
class ContestUsers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['firstname', 'lastname', 'birthday', ], 'required','message' => 'Поле {attribute} не должно быть пустым'],
            [['birthday'], 'safe'],
            [['birthday'], 'date', 'format' => 'yyyy-MM-dd','message'=>'Формат даты должен быть в виде "ГГГГ-ММ-ДД"'],
            [['role', 'organizationkey'], 'integer'],
            [['firstname', 'secondname', 'lastname', 'username', 'phonenumber', 'email'], 'string', 'max' => 30,'message' => 'Максимальный размер для поля {attribute} 30 символов'],
            ['email', 'email','message' => 'Введите правильный email адрес'],
            [['organizationkey'], 'exist', 'skipOnError' => true, 'targetClass' => Organizations::className(), 'targetAttribute' => ['organizationkey' => 'organizationkey']],
            [['email'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'id',
            'firstname' => 'Имя',
            'secondname' => 'Отчество',
            'lastname' => 'Фамилия',
            'birthday' => 'Дата рождения',
            'username' => 'Имя пользователя',
            'role' => 'Роль',
            'phonenumber' => 'Номер телефона',
            'email' => 'Email',
            'organizationkey' => 'Организация',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrganizationkey0()
    {
        return $this->hasOne(Organizations::className(), ['organizationkey' => 'organizationkey']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequests()
    {
        return $this->hasMany(Requests::className(), ['teacherkey' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequests0()
    {
        return $this->hasMany(Requests::className(), ['id' => 'id']);
    }
}
