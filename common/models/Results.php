<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "results".
 *
 * @property integer $resultkey
 * @property integer $mark
 * @property integer $ajudicatorkey
 * @property integer $requestkey
 *
 * @property Requests $requestkey0
 * @property Ajudicators $ajudicatorkey0
 */
class Results extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'results';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mark', 'ajudicatorkey', 'requestkey'], 'required', 'message' => 'Поле {attribute} не должно быть пустым'],
            [['mark', 'ajudicatorkey', 'requestkey'], 'integer','message' => 'Поле {attribute} должно быть числом'],
            [['requestkey'], 'exist', 'skipOnError' => true, 'targetClass' => Requests::className(), 'targetAttribute' => ['requestkey' => 'requestkey']],
            [['ajudicatorkey'], 'exist', 'skipOnError' => true, 'targetClass' => Ajudicators::className(), 'targetAttribute' => ['ajudicatorkey' => 'ajudicatorkey']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'resultkey' => 'Resultkey',
            'mark' => 'Оценка',
            'ajudicatorkey' => 'Проверяющий',
            'requestkey' => 'Заявка №',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequestkey0()
    {
        return $this->hasOne(Requests::className(), ['requestkey' => 'requestkey']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAjudicatorkey0()
    {
        return $this->hasOne(Ajudicators::className(), ['ajudicatorkey' => 'ajudicatorkey']);
    }
}
