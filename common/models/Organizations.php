<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "organizations".
 *
 * @property integer $organizationkey
 * @property string $organizationname
 * @property string $location
 * @property string $phonenumber
 * @property string $email
 *
 * @property ContestUsers[] $contestUsers
 * @property User[] $users
 */
class Organizations extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'organizations';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['organizationname'], 'required', 'message' => 'Поле {attribute} не должно быть пустым'],
            [['organizationname'], 'string', 'max' => 50,'message' => 'Максимальный размер для поля {attribute} 50 символов'],
            [['location', 'phonenumber', 'email'], 'string', 'max' => 100,'message' => 'Максимальный размер для поля {attribute} 100 символов'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'organizationkey' => 'Organizationkey',
            'organizationname' => 'Название организации',
            'location' => 'Адрес',
            'phonenumber' => 'Номер телефона',
            'email' => 'Email',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContestUsers()
    {
        return $this->hasMany(ContestUsers::className(), ['organizationkey' => 'organizationkey']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['organizationkey' => 'organizationkey']);
    }


    /**
     * Finds organization by name
     *
     * @param string $name
     * @return static|null
     */
    public static function findByName($organizationname)
    {
        return static::findOne(['organizationname' => $organizationname]);
    }
}
