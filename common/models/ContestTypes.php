<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "contest_types".
 *
 * @property integer $contesttypekey
 * @property string $name
 * @property string $comment
 *
 * @property Contests[] $contests
 */
class ContestTypes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contest_types';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required', 'message' => 'Поле {attribute} не должно быть пустым'],
            [['name'], 'string', 'max' => 20,'message' => 'Максимальный размер для поля {attribute} 20 символов'],
            [['comment'], 'string', 'max' => 200,'message' => 'Максимальный размер для поля {attribute} 200 символов'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'contesttypekey' => 'Тип конкурса',
            'name' => 'Наименование',
            'comment' => 'Комментарии',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContests()
    {
        return $this->hasMany(Contests::className(), ['contesttypekey' => 'contesttypekey']);
    }
}
