<?php

namespace common\models;

use Yii;
use yii\web\UploadedFile;


/**
 * This is the model class for table "contests".
 *
 * @property integer $contestkey
 * @property string $contestname
 * @property string $condition
 * @property string $datestart
 * @property string $datefinish
 * @property integer $lowage
 * @property integer $hightage
 * @property integer $contesttypekey
 *
 * @property Ajudicators[] $ajudicators
 * @property ContestTypes $contesttypekey0
 * @property Requests[] $requests
 */
class Contests extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public $file;

    public static function tableName() {
        return 'contests';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['contestname', 'datestart', 'datefinish', 'lowage', 'hightage', 'contesttypekey'], 'required', 'message' => 'Поле {attribute} не должно быть пустым'],
            [['datestart', 'datefinish'], 'safe'],
            [['datestart', 'datefinish'], 'date', 'format' => 'yyyy-MM-dd','message'=>'Формат даты должен быть в виде "ГГГГ-ММ-ДД"'],
            [['lowage', 'hightage', 'contesttypekey'], 'integer', 'message' => 'Поле {attribute} должно быть числом'],
            [ ['lowage'],'number','min'=>5,'message' => 'Поле {attribute} должно быть числом больше 5','tooSmall'=>'Поле {attribute} должно быть числом больше 5'],
            [['hightage'], 'number','max'=>99,'message' => 'Поле {attribute} должно быть числом меньше 100','tooBig'=>'Поле {attribute} должно быть числом меньше 100'],
            //['hightage', 'compare', 'compareValue' => $this->lowage, 'operator' => '>='],
            [['contestname'], 'string', 'max' => 30, 'message' => 'Максимальный размер для поля {attribute} 30 символов'],
            [['condition'], 'string', 'max' => 100, 'message' => 'Максимальный размер для поля {attribute} 100 символов'],
            [['contesttypekey'], 'exist', 'skipOnError' => true, 'targetClass' => ContestTypes::className(), 'targetAttribute' => ['contesttypekey' => 'contesttypekey']],
            [['datestart','datefinish'],'validateDate'],
            [['lowage','hightage'],'validateAge'],
            [['file'], 'file','extensions' => 'doc, docx, pdf, rtf','message' => 'Загрузите работу','wrongExtension'=>'Ошибка формата.Разрешенные форматы: png, jpg, doc, docx, ppt, pptx, pdf, mp4, mp3','uploadRequired'=>'Файл не загружен'],
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'contestkey' => 'Contestkey',
            'contestname' => 'Название',
            'condition' => 'Положение',
            'datestart' => 'Дата начала',
            'datefinish' => 'Дата окончания',
            'lowage' => 'Минимальный возраст',
            'hightage' => 'Максимальный возраст',
            'contesttypekey' => 'Тип конкурса',
            'file'=>'Файл работы',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAjudicators() {
        return $this->hasMany(Ajudicators::className(), ['contestkey' => 'contestkey']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContesttypekey0() {
        return $this->hasOne(ContestTypes::className(), ['contesttypekey' => 'contesttypekey']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function validateDate($attribute)
    {
        $lowage_atr=  strtotime($this->datestart);
         $highage_atr=  strtotime($this->datefinish);
        
        if ($lowage_atr>=$highage_atr) {
            $this->addError($attribute, 'Поле "Дата начала" должно быть меньше поля "Дата окончания"');
        }
    }
    public function validateAge($attribute)
    {
        $lowage_atr=  (int)$this->lowage;
         $highage_atr=  (int)$this->hightage;
        
        if ($lowage_atr>=$highage_atr) {
            $this->addError($attribute, 'Поле "Минимальный возоаст" должно быть меньше поля "Максимальный возоаст"');
        }
    }
    public function beforeValidate() {
        if($this->isNewRecord){
                $this->file = UploadedFile::getInstance($this, 'file');
        }
       return parent::beforeValidate();
    }
}
