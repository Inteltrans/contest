<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "ajudicators".
 *
 * @property integer $ajudicatorkey
 * @property integer $contestkey
 * @property integer $userkey
 *
 * @property Contests $contestkey0
 * @property User $userkey0
 * @property Results[] $results
 */
class Ajudicators extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ajudicators';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['contestkey', 'userkey'], 'required','message' => 'Поле {attribute} не должно быть пустым'],
            [['contestkey', 'userkey'], 'integer'],
            [['contestkey'], 'exist', 'skipOnError' => true, 'targetClass' => Contests::className(), 'targetAttribute' => ['contestkey' => 'contestkey']],
            [['userkey'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['userkey' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ajudicatorkey' => 'Ajudicatorkey',
            'contestkey' => 'Конкурс',
            'userkey' => 'Пользователь',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContestkey0()
    {
        return $this->hasOne(Contests::className(), ['contestkey' => 'contestkey']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserkey0()
    {
        return $this->hasOne(User::className(), ['id' => 'userkey']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResults()
    {
        return $this->hasMany(Results::className(), ['ajudicatorkey' => 'ajudicatorkey']);
    }
}
