<?php
return [
    'dashboard' => [
        'type' => 2,
        'description' => 'Админ панель',
    ],
    'user' => [
        'type' => 1,
        'description' => 'Пользователь',
        'ruleName' => 'userRole',
        'children' => [
            'updateOwn',
            'actionContest',
            'actionResult',
        ],
    ],
    'moder' => [
        'type' => 1,
        'description' => 'Модератор',
        'ruleName' => 'userRole',
        'children' => [
            'user',
            'dashboard',
            'updateOwn',
            'actionContest',
            'actionResult',
        ],
    ],
    'admin' => [
        'type' => 1,
        'description' => 'Администратор',
        'ruleName' => 'userRole',
        'children' => [
            'moder',
            'updateOwn',
            'actionContest',
            'actionResult',
        ],
    ],
    'updateOwn' => [
        'type' => 2,
        'ruleName' => 'isOwner',
    ],
    'actionContest' => [
        'type' => 2,
        'ruleName' => 'actionContest',
    ],
    'actionResult' => [
        'type' => 2,
        'ruleName' => 'actionResult',
    ],
];
