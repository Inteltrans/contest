<?php
namespace common\components\rbac;

use common\models\User;
use yii\helpers\ArrayHelper;
use yii\rbac\Rule;
use yii\rbac\Item;

class ActionResultsRule extends Rule
{
    public $name = 'actionResult';

    /**
     * @param string|integer $user   the user ID.
     * @param Item           $item   the role or permission that this rule is associated with
     * @param array          $params parameters passed to ManagerInterface::checkAccess().
     *
     * @return boolean a value indicating whether the rule permits the role or permission it is associated with.
     */
    public function execute($user, $item, $params)
    {
        return isset($params['post'])
            ? $params['post'] == User::ROLE_ADMIN
                || $params['post'] == User::ROLE_MODER
            : false;
    }
}