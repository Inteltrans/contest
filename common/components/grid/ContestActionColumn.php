<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 19.12.16
 * Time: 22:18
 */

namespace common\components\grid;


use Yii;
use yii\helpers\Html;
use yii\grid\ActionColumn;

class ContestActionColumn extends ActionColumn
{
    protected function initDefaultButtons()
    {
        if (!isset($this->buttons['view'])) {
            $this->buttons['view'] = function ($url, $model, $key) {
                $options = array_merge([
                    'title' => Yii::t('yii', 'View'),
                    'aria-label' => Yii::t('yii', 'View'),
                    'data-pjax' => '0',
                ], $this->buttonOptions);
                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, $options);
            };
        }

        $userId = Yii::$app->user->identity->getId();
        $sql = "SELECT role FROM user WHERE $userId=user.id;";
        $command = Yii::$app->db->createCommand($sql);
        $role = $command->queryScalar();

        if (!isset($this->buttons['update']) && Yii::$app->user->can('actionContest', ['post' => $role])) {
            $this->buttons['update'] = function ($url, $model, $key) {
                $options = array_merge([
                    'title' => Yii::t('yii', 'Update'),
                    'aria-label' => Yii::t('yii', 'Update'),
                    'data-pjax' => '0',
                ], $this->buttonOptions);
                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, $options);
            };
        }
        if (!isset($this->buttons['delete'])&& Yii::$app->user->can('actionContest', ['post' => $role])) {
            $this->buttons['delete'] = function ($url, $model, $key) {
                $options = array_merge([
                    'title' => Yii::t('yii', 'Delete'),
                    'aria-label' => Yii::t('yii', 'Delete'),
                    'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                    'data-method' => 'post',
                    'data-pjax' => '0',
                ], $this->buttonOptions);
                return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, $options);
            };
        }
    }
}