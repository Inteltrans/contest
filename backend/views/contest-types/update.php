<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ContestTypes */

$this->title = 'Обновить тип конкурса: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Типы конкурсов', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->contesttypekey]];
$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="contest-types-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
