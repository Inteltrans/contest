<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ContestTypes */

$this->title = 'Создание нового типа конкурса';
$this->params['breadcrumbs'][] = ['label' => 'Типы конкурсов', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contest-types-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
