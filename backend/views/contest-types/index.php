<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ContestTypesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Типы конкурсов';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contest-types-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать новый тип конкурса', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            'comment',

            ['class' => 'yii\grid\ActionColumn'],
        ],
        'summary' => "",
    ]); ?>
</div>
