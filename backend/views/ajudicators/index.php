<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AjudicatorsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Проверяющие';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ajudicators-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать проверяющего', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'contestkey',      
            'userkey',
            ['class' => 'yii\grid\ActionColumn'],
        ],
         'summary' => "",
    ]); ?>
</div>
