<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Ajudicators */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ajudicators-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <?= $form->field($model, 'contestkey')->dropDownList($items_contests,$params_contests) ?>
    
    <?= $form->field($model, 'userkey')->dropDownList($items_users,$params_users) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
