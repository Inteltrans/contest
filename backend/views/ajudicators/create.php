<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Ajudicators */

$this->title = 'Создание проверяющего';
$this->params['breadcrumbs'][] = ['label' => 'Проверяющие', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ajudicators-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'items_contests'=>$items_contests,
        'params_contests'=>$params_contests,
       'items_users'=> $items_users,
       'params_users'=> $params_users,
    ]) ?>

</div>
