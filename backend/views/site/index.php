<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
$this->title = 'My Yii Application';
?>
<div class="site-index">

       <div class="jumbotron">
        <h1>Добро пожаловать на сайт конкурсов!</h1>

    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>Конкурсы</h2>

                <p>Список текущих конкурсов</p>

                <p><?= Html::a('Конкурсы', ['/contests/index'],['class'=>'btn btn-default']) ?></p>
            </div>
            <div class="col-lg-4">
                <h2>Регистрация</h2>

                <p>Пройдите регистрацию по данной ссылке для участия в конкурсах</p>

                <p><?= Html::a('Регистрация', ['/site/signup'],['class'=>'btn btn-default']) ?></p>
            </div>
            <div class="col-lg-4">
                <h2>Результаты проведенных конкурсов</h2>

                <p>Пройдите по данной ссылке чтобы увидеть результаты прошедших конкурсов</p>

                <p><?= Html::a('Результаты', ['/results/index'],['class'=>'btn btn-default']) ?></p>
            </div>
        </div>

    </div>
</div>
