<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Results */

$this->title = 'Обновить результат: ' . $model->resultkey;
$this->params['breadcrumbs'][] = ['label' => 'Результаты', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->resultkey, 'url' => ['view', 'id' => $model->resultkey]];
$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="results-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
