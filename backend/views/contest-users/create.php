<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ContestUsers */

$this->title = 'Создать участника';
$this->params['breadcrumbs'][] = ['label' => 'Участники', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contest-users-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'items' => $items,
        'params'=>$params,
    ]) ?>

</div>
