<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ContestUsers */

$this->title = 'Обновить участника: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Участники', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="contest-users-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'items' => $items,
        'params'=>$params,
    ]) ?>

</div>
