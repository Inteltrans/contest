<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\ContestUsers */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="contest-users-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'firstname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'secondname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'lastname')->textInput(['maxlength' => true]) ?>

    <?=
    $form->field($model, 'birthday')->textInput()->widget(DatePicker::className(), array(
        'language' => 'ru',
        'dateFormat' => 'yyyy-MM-dd',
            )
    );
    ?>

    <?= $form->field($model, 'phonenumber')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'organizationkey')->dropDownList($items,$params) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать участника' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
