<?php

use yii\helpers\Html;



/* @var $this yii\web\View */
/* @var $model app\models\Contests */

$this->title = 'Создание конкурса';
$this->params['breadcrumbs'][] = ['label' => 'Конкурсы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?php
use yii\widgets\ActiveForm;
?>

<div class="contests-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'items' => $items,
        'params'=>$params,
    ]) ?>

</div>
