<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\jui\DatePicker;
/* @var $this yii\web\View */
/* @var $model app\models\Contests */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="contests-form">

    <?php $form = ActiveForm::begin( ['options' => ['enctype' => 'multipart/form-data']]); ?>
    
    <?= $form->field($model, 'contestname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'file')->fileInput() ?>

    <?=
    $form->field($model, 'datestart')->textInput()->widget(DatePicker::className(), array(
        'language' => 'ru',
        'dateFormat' => 'yyyy-MM-dd',
            )
    );
    ?>


    <?=
    $form->field($model, 'datefinish')->textInput()->widget(DatePicker::className(), array(
        'language' => 'ru',
        'dateFormat' => 'yyyy-MM-dd',
            )
    );
    ?>

    <?= $form->field($model, 'lowage')->textInput() ?>

    <?= $form->field($model, 'hightage')->textInput() ?>

    <?= $form->field($model, 'contesttypekey')->dropDownList($items,$params) ?>
    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    
    
    <?php ActiveForm::end(); ?>

</div>
