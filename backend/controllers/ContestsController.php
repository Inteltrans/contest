<?php

namespace backend\controllers;

use Yii;
use common\models\Contests;
use common\models\ContestsSearch;
use common\models\ContestTypes;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;




/**
 * ContestsController implements the CRUD actions for Contests model.
 */
class ContestsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Contests models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ContestsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Contests model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Contests model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Contests();  
        if( $model->load(Yii::$app->request->post()) && $model->save()){
            $model->condition = Yii::$app->basePath . '/uploads/' . $model->contestkey . '_' . $model->contesttypekey . '.' . $model->file->extension;
			$model->update();
			if ($model->validate()) {
				$model->file->saveAs($model->condition);
			}
            return $this->redirect(['view', 'id' => $model->contestkey]);
        }
        else { 
            $contests_types = ContestTypes::find()->all();
            $items = ArrayHelper::map($contests_types,'contesttypekey','name');
            $params = [
                'prompt' => 'Выберите конкурс'
            ];
            return $this->render('create', [
                'model' => $model,
                'items' => $items,
                'params'=>$params,
            ]);
        }
    }

    /**
     * Updates an existing Contests model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            return $this->redirect(['view', 'id' => $model->contestkey]);
        } else {
            $contests_types = ContestTypes::find()->all();
            $items = ArrayHelper::map($contests_types,'contesttypekey','name');
            $params = [
                'prompt' => 'Выберите тип конкурса'
            ];
            return $this->render('update', [
                'model' => $model,
                'items' => $items,
                'params'=>$params,
            ]);
        }
    }

    public function actionDownload ($id)
	{
		$model = $this->findModel($id);
		$path = $model->condition;
			if (file_exists($path)) {
			return Yii::$app->response->sendFile($path);
		}
	}
    /**
     * Deletes an existing Contests model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Contests model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Contests the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Contests::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
