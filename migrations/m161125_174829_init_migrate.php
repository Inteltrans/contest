<?php

use yii\db\Migration;

class m161125_174829_init_migrate extends Migration {

    public function up() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
     
         $this->createTable('{{%ajudicators}}', [
            'ajudicatorkey' => 'pk',
            'contestkey' => 'int(11) NOT NULL',
            'id' => 'int(11) NOT NULL',
                ], $tableOptions);




        $this->createTable('{{%contest_types}}', [
            'contesttypekey' => 'pk',
            'name' => 'VARCHAR(20) NOT NULL',
            'comment' => 'VARCHAR(200)',
                ], $tableOptions);

        $this->createTable('{{%organizations}}', [
            'organizationkey' => 'pk',
            'organizationname' => 'VARCHAR(50) NOT NULL',
            'location' => 'VARCHAR(100)',
            'phonenumber' => 'VARCHAR(100)',
            'email' => 'VARCHAR(100)',
                ], $tableOptions);

        $this->createTable('{{%requests}}', [
            'requestkey' => 'pk',
            'worklink' => 'VARCHAR(100) NOT NULL',
            'contestkey' => 'int(11) NOT NULL',
            'id' => 'int(11) NOT NULL',
            'teacherkey' => 'int(11) NOT NULL',
                ], $tableOptions);


        $this->createTable('{{%results}}', [
            'resultkey' => 'pk',
            'mark' => 'int(11) NOT NULL',
            'ajudicatorkey' => 'int(11) NOT NULL',
            'requestkey' => 'int(11) NOT NULL',
                ], $tableOptions);

        
        $this->createTable('{{%contests}}', [
             'contestkey' => 'pk',
            'contestname' => 'VARCHAR(30) NOT NULL',
            'condition' => 'VARCHAR(100)NOT NULL',
            'datestart' => 'DATE NOT NULL',
            'datefinish' => 'DATE NOT NULL',
            'lowage' => 'int(2) NOT NULL',
            'hightage' => 'int(2) NOT NULL',
            'contesttypekey' => 'int(11) NOT NULL',
                ], $tableOptions);
        
        
        $this->createTable('{{%contest_users}}', [
             'id' => 'pk',
            'firstname' => 'VARCHAR(30) NOT NULL',
            'secondname' => 'VARCHAR(30)',
            'lastname' => 'VARCHAR(30) NOT NULL',
            'birthday' => 'DATE NOT NULL',
            'login' => 'VARCHAR(30) NOT NULL',
            'password' => 'VARCHAR(30) NOT NULL',
            'role' => 'int(1) NOT NULL',
            'phonenumber' => 'VARCHAR(30)',
            'email' => 'VARCHAR(30)',
            'organizationkey' => 'int(11) NOT NULL',
                ], $tableOptions);






        $this->addForeignKey('fk_results_user', '{{%results}}', 'ajudicatorkey', '{{%ajudicators}}', 'ajudicatorkey', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_results_teacher', '{{%results}}', 'requestkey', '{{%requests}}', 'requestkey', 'CASCADE', 'CASCADE');

        $this->addForeignKey('fk_ajudicators_contest', '{{%ajudicators}}', 'contestkey', '{{%contests}}', 'contestkey', 'CASCADE', 'CASCADE');
       
        $this->addForeignKey('fk_ajudicators_user', '{{%ajudicators}}', 'userkey', '{{%user}}', 'id', 'CASCADE', 'CASCADE');
         $this->addForeignKey('fk_contest_contest_type', '{{%contests}}', 'contesttypekey', '{{%contest_types}}', 'contesttypekey', 'CASCADE', 'CASCADE');

        $this->addForeignKey('fk_requests_contest', '{{%requests}}', 'contestkey', '{{%contests}}', 'contestkey', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_request_user', '{{%requests}}', 'id', '{{%contest_users}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_request_teacher', '{{%requests}}', 'teacherkey', '{{%contest_users}}', 'id', 'CASCADE', 'CASCADE');

        $this->addForeignKey('fk_contest_users_organization', '{{%contest_users}}', 'organizationkey', '{{%organizations}}', 'organizationkey', 'CASCADE', 'CASCADE');
    }

    public function down() {
        echo "m161125_174829_init_migrate cannot be reverted.\n";

        return false;
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
