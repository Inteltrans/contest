<?php

namespace frontend\controllers;

use common\models\Ajudicators;
use Yii;
use common\models\Results;
use common\models\ResultsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use kartik\mpdf\Pdf;

/**
 * ResultsController implements the CRUD actions for Results model.
 */
class ResultsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionReportdocx($id){
        $content = $this->getHtmlReport($id);
        return Yii::$app->response->sendContentAsFile($content, "Диплом.docx");
    }

    public function actionReportdoc($id){
        $content = $this->getHtmlReport($id);
        return Yii::$app->response->sendContentAsFile($content, "Диплом.doc");
    }
    public function actionReportpdf($id) {
        $content = $this->getHtmlReport($id);
               // setup kartik\mpdf\Pdf component
        $pdf = new Pdf([
            // set to use core fonts only
            'mode' => Pdf::MODE_UTF8,
            // A4 paper format
            'format' => Pdf::FORMAT_A4,
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
            // your html content input
            'content' => $content,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            // any css to be embedded if required
            //https://mpdf.github.io/what-else-can-i-do/backgrounds-borders.html
            'cssInline' => '.kv-heading-1{font-size:18px}
                            html { height: 100%;width:100%; }
                            body {
                                    margin: 0; /* Убираем отступы */
                                    height: 100%; /* Высота страницы */
                                    background: url(http://fotoknigin.ru/wp-content/uploads/2012/03/gramota1-dyplom1.jpg)no-repeat center center fixed;
                                    background-image-resize:6;
                                 }',
            // set mPDF properties on the fly
            'options' => ['title' => 'Диплом'],
            // call mPDF methods on the fly
        ]);
         $pdf->render();
         exit;
    }
    /**
     * Lists all Results models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ResultsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Results model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Results model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Results();

        $userKey= Yii::$app->user->identity->getId();
        $sql = "SELECT ajudicatorkey FROM ajudicators WHERE userkey=$userKey;";
        $command = Yii::$app->db->createCommand($sql);
        $model->ajudicatorkey = $command->queryScalar();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->resultkey]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Results model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->resultkey]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Results model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Results model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Results the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Results::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @param $id
     */
    private function getReportInfo($id)
    {
        $contestId = $this->getContestId($id);
        $sql = "SELECT
                  resultkey,
                  user,
                  organizationname,
                  location,
                  contestname,
                  mark
                    FROM results,requests,contests,organizations
                      WHERE requests.requestkey=results.requestkey
                        AND organizations.organizationkey=requests.organizationkey
                        AND contests.contestkey=requests.contestkey
                        AND $contestId=contests.contestkey
                    ORDER BY mark DESC;";
        $command = Yii::$app->db->createCommand($sql);
        $dataReader = $command->query();
        $place = 1;
        foreach($dataReader as $row) {
            if($row['resultkey']==$id)
                return [
                        'place' => $place,
                        'info' => $row
                        ];
            $place++;
        }
    }

    private function getContestId($resultId){
        $sql = "SELECT contests.contestkey as 'contestId' 
                    FROM results 
                      left join requests on results.requestkey = requests.requestkey
                        left join contests on requests.contestkey = contests.contestkey
                    WHERE $resultId=resultkey";
        $command = Yii::$app->db->createCommand($sql);
        return $command->queryScalar();
    }

    /**
     * @param $id
     * @return string
     */
    private function getHtmlReport($id)
    {
        $reportInfo = $this->getReportInfo($id);
        // get your HTML raw content without any layouts or scripts
        $place = $reportInfo['place'];
        $info = $reportInfo['info'];

        $userName = $info['user'];

        $organizationName = $info['organizationname'];
        $location = $info['location'];
        $contestName = $info['contestname'];

        $content = "<html>
                        <body>
                            <br/>
                            <br/>
                            <br/>
                            <br/>
                            <br/>
                            <br/>
                            <br/>
                            <br/>
                            <br/>
                            <br/>
                            <br/>
                            <br/>
                            <br/>
                            <br/>
                            <br/>
                            <h2 align='center'>Выдается $userName</h2>
                            <h2 align='center'>из $organizationName города $location</h2>
                            <h2 align='center'>за $place место в конкурсе \"$contestName\"</h2>
                        </body>
                    </html>";
        return $content;
    }
}
