<?php

namespace frontend\controllers;

use Yii;
use common\models\Ajudicators;
use common\models\AjudicatorsSearch;
use common\models\Contests;
use common\models\User;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * AjudicatorsController implements the CRUD actions for Ajudicators model.
 */
class AjudicatorsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Ajudicators models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AjudicatorsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Ajudicators model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Ajudicators model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Ajudicators();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->ajudicatorkey]);
        } else {
            $contests = Contests::find()->all();
            $items_contests = ArrayHelper::map($contests,'contestkey','contestname');
            $params_contests = [
                'prompt' => 'Выберите конкурс'
            ];
            $users = User::find()->all();
            $items_users = ArrayHelper::map($users,'id','lastname');
            $params_users = [
                'prompt' => 'Выберите пользователя'
            ];
            return $this->render('create', [
                'model' => $model,
                'items_contests'=>$items_contests,
                'params_contests'=>$params_contests,
                'items_users'=>$items_users,
                'params_users'=>$params_users,
            ]);
        }
    }

    /**
     * Updates an existing Ajudicators model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->ajudicatorkey]);
        } else {
            $contests = Contests::find()->all();
            $items_contests = ArrayHelper::map($contests,'contestkey','contestname');
            $params_contests = [
                'prompt' => 'Выберите конкурс'
            ];
            $users = User::find()->all();
            $items_users = ArrayHelper::map($users,'id','lastname');
            $params_users = [
                'prompt' => 'Выберите пользователя'
            ];
            return $this->render('update', [
                'model' => $model,
                'items_contests'=>$items_contests,
                'params_contests'=>$params_contests,
                'items_users'=>$items_users,
                'params_users'=>$params_users,
            ]);
        }
    }

    /**
     * Deletes an existing Ajudicators model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Ajudicators model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Ajudicators the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Ajudicators::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
