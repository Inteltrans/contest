<?php

namespace frontend\controllers;

use common\models\Organizations;
use Yii;
use common\models\Requests;
use common\models\RequestsSearch;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use common\models\Contests;
use common\models\ContestUsers;
use yii\helpers\ArrayHelper;

/**
 * RequestsController implements the CRUD actions for Requests model.
 */
class RequestsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Requests models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RequestsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Requests model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Requests model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Requests();
        $model->user_owner_key = Yii::$app->user->identity->getId();

        if ($model->load(Yii::$app->request->post())&&$model->save()) {
			$model->worklink = Yii::$app->basePath . '/uploads/works/' . 'work' . $model->requestkey . '_' . $model->contestkey . '_' . time() . '.' . $model->file->extension;
			$model->update();
			if ($model->validate()) {
				$model->file->saveAs($model->worklink);
			}

			return $this->redirect(['view', 'id' => $model->requestkey]);
        } else {
            $organizationsparam = ['prompt' => 'Выберите организацию'];
            $organizations_ = Organizations::find()->all();
            $organizations = ArrayHelper::map($organizations_,'organizationkey','organizationname');

            $contestparam = ['prompt' => 'Выберите конкурс'];
            $contests_ = Contests::find()->all();
            $contests = ArrayHelper::map($contests_,'contestkey','contestname');

            $userparam = ['prompt' => 'Выберите пользователя'];
            $user = ContestUsers::findOne(Yii::$app->user->identity->getId());
            $users_ = ContestUsers::find()->where("organizationkey=($user->organizationkey)")->all();
            $users = ArrayHelper::map($users_,'id','lastname');
            return $this->render('create', [
                'model' => $model,
                'contestparam' =>$contestparam,
                'contests'=>$contests,
                'users'=>$users,
                'userparam'=>$userparam,
                'organizations'=>$organizations,
                'organizationparam' => $organizationsparam
            ]);
        }
    }

    /**
     * Updates an existing Requests model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if (!\Yii::$app->user->can('updateOwn', ['post' => $model->userkey])) {
            throw new ForbiddenHttpException('Доступ запрещен');
        }

        if ($model->load(Yii::$app->request->post())&&$model->save()) {

            $model->file = UploadedFile::getInstance($model, 'file');
			
			$model->worklink = Yii::$app->basePath . '/uploads/works/' . 'work' . $model->requestkey . '_' . $model->contestkey . '_' . $model->userkey . '.' . $model->file->extension;
			$model->update();
			if ($model->validate()) {
				$model->file->saveAs($model->worklink);
			}
                        return $this->redirect(['view', 'id' => $model->requestkey]);
          
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
	
	public function actionDownload ($id)
	{
		$model = $this->findModel($id);
		$path = $model->worklink;
			if (file_exists($path)) {
			return Yii::$app->response->sendFile($path);
		}
	}
	
    /**
     * Deletes an existing Requests model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Requests model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Requests the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Requests::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
