<?php
namespace frontend\models;

use common\models\Organizations;
use yii\base\Model;
use common\models\User;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $firstname;
    public $secondname;
    public $lastname;
    public $organization;
    public $birthday;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'Это имя уже занято.'],
            ['username', 'string', 'min' => 2, 'max' => 30],

            ['firstname', 'trim'],
            ['firstname', 'required'],
            ['firstname', 'string', 'min' => 2, 'max' => 30],

            ['secondname', 'trim'],
            ['secondname', 'required'],
            ['secondname', 'string', 'min' => 2, 'max' => 30],

            ['lastname', 'trim'],
            ['lastname', 'required'],
            ['lastname', 'string', 'min' => 2, 'max' => 30],

            ['birthday', 'trim'],
            ['birthday', 'required'],

            ['organization','required'],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'Этот email уже занят.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $user = new User();
        $user->username = $this->username;
        $user->email = $this->email;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        $user->firstname = $this->firstname;
        $user->secondname = $this->secondname;
        $user->lastname = $this->lastname;
        $user->birthday = $this->birthday;
        $user->role = User::ROLE_USER;

        $user->organizationkey = $this->organization;

        return $user->save() ? $user : null;
    }
    public function attributeLabels() {
        return [
            'username' => 'Имя пользователя',
            'password' => 'Пароль',
            'firstname' => 'Имя',
            'secondname' => 'Отчество',
            'lastname' => 'Фамилия',
            'organization' => 'Организация',
            'birthday' => 'Дата рождения'
        ];
    }
}
