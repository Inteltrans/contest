<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Results */

$this->title = 'Новая оценка';
$this->params['breadcrumbs'][] = ['label' => 'Результаты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="results-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
