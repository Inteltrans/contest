<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ResultsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Результаты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="results-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php
        $userId = Yii::$app->user->identity->getId();
        $sql = "SELECT role FROM user WHERE $userId=user.id;";
        $command = Yii::$app->db->createCommand($sql);
        $role = $command->queryScalar();
        $canEditCreate = Yii::$app->user->can('actionContest', ['post' => $role]);

		if(Yii::$app->user->isGuest)
			echo "Необходимо зарегистрироваться.";
		elseif ($canEditCreate) {
            echo Html::a('Оценить работу', ['create'], ['class' => 'btn btn-success']);
        }
        ?>
    </p>
    <?php
		if(!Yii::$app->user->isGuest)
		{
			echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'mark',
            'requestkey',

            ['class' => 'common\components\grid\ResultActionColumn']
        ],
         'summary' => "",
    ]); 
		}?>
</div>
