<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Results */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="results-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'mark')->textInput() ?>

    <?php
    $requests = \common\models\Requests::find()->all();

    $items = \yii\helpers\ArrayHelper::map($requests,'requestkey','requestkey');
    ?>
    <?= $form->field($model, 'requestkey')->dropDownList($items)?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Новая оценка' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
