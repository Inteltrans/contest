<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Results */

$this->title ="Результат";
$this->params['breadcrumbs'][] = ['label' => 'Результаты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="results-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php
        $userId = Yii::$app->user->identity->getId();
        $sql = "SELECT role FROM user WHERE $userId=user.id;";
        $command = Yii::$app->db->createCommand($sql);
        $role = $command->queryScalar();
        $canEditCreate = Yii::$app->user->can('actionContest', ['post' => $role]);
        if ($canEditCreate) {
            echo Html::a('Обновить', ['update', 'id' => $model->resultkey], ['class' => 'btn btn-primary']);

            echo Html::a('Удалить', ['delete', 'id' => $model->resultkey], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Вы действительно хотите удалить данный результат?',
                    'method' => 'post',
                ],
            ]);
        }
        ?>

        <?= Html::a('Скачать диплом',['reportpdf','id' => $model->resultkey],[
                'class' => 'btn btn-primary',
                'target'=>'_blank',
                'data-toggle'=>'tooltip',
                'title'=>'Откроет диплом в новом окне'
        ])?>
    </p>

    <?php
    $ajudicatorkey = $model->ajudicatorkey;
    $sql = "SELECT userkey FROM ajudicators WHERE ajudicatorkey=$ajudicatorkey;";
    $command = Yii::$app->db->createCommand($sql);
    $userKey = $command->queryScalar();

    $sql = "SELECT firstname, secondname, lastname FROM user WHERE id=$userKey;";
    $command = Yii::$app->db->createCommand($sql);
    $queryAnswer = $command->queryOne();

    $firstname = $queryAnswer['firstname'];
    $secondname = $queryAnswer['secondname'];
    $lastname = $queryAnswer['lastname'];

    $ajudicator = "$firstname $secondname $lastname";

    $requestKey = $model->requestkey;
    $linkToRequest = "/requests/view";
    $buttonWithLink = Html::a($requestKey, [$linkToRequest, 'id' => $requestKey], ['class'=>'btn btn-default']);
    echo DetailView::widget([
        'model' => $model,
        'attributes' => [
            'mark',
            [
                'attribute' => 'ajudicatorkey',
                'value' => $ajudicator
            ],
            [
                'attribute' => 'requestkey',
                'format' => 'raw',
                'value' => $buttonWithLink
            ],
        ],
    ]) ?>

</div>
