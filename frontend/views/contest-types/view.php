<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ContestTypes */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Типы конкурсов', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contest-types-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Обновить', ['update', 'id' => $model->contesttypekey], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->contesttypekey], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно хотите удалить данный тип конкурса?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            'comment',
        ],
    ]) ?>

</div>
