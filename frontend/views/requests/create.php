<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Requests */

$this->title = 'Подача заявки';
$this->params['breadcrumbs'][] = ['label' => 'Заявки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="requests-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'contests'=> $contests,
        'contestparam'=>$contestparam,
        'users'=>$users,
        'userparam'=>$userparam,
        'organizations'=>$organizations,
        'organizationparam' => $organizationparam
    ]) ?>

</div>
