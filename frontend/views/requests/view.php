<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Requests */

$this->title = "Заявка";
$this->params['breadcrumbs'][] = ['label' => 'Заявки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="requests-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Обновить', ['update', 'id' => $model->requestkey], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->requestkey], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно хотите удалить данную заявку?',
                'method' => 'post',
            ],
        ]) ?>
		<?= Html::a('Скачать работу', ['download', 'id' => $model->requestkey], ['class' => 'btn btn-primary']) ?>
    </p>

    <?php

    $sql = "SELECT contestname FROM contests WHERE contestkey=$model->contestkey;";
    $command = Yii::$app->db->createCommand($sql);
    $contestName = $command->queryScalar();

    echo DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'contestkey',
                'value' => $contestName
            ],
            'user',
            'teacher',
        ],
    ]) ?>

</div>
