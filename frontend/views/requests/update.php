<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Requests */

$this->title = 'Обновить заявку: ' . $model->requestkey;
$this->params['breadcrumbs'][] = ['label' => 'Заявки', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->requestkey, 'url' => ['view', 'id' => $model->requestkey]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="requests-update">

    <h1><?= Html::encode($this->title) ?></h1>

	<?= Html::a('Скачать работу', ['download', 'id' => $model->requestkey], ['class' => 'btn btn-primary']) ?>
	
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
