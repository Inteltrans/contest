<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\jui\DatePicker;
use yii\widgets\MaskedInput;

$this->params['breadcrumbs'][] = ['label'=>'Основное', 'url'=> Yii::$app->urlManager->createUrl(['site/personalarea'])];
$this->params['breadcrumbs'][] = ['label'=>'Настройки', 'url'=> Yii::$app->urlManager->createUrl(['site/password-change'])];

?>
<div class="user-profile-update">
 
    <h1><?= Html::encode($this->title) ?></h1>
 
    <div class="user-form">
 
        <?php $form = ActiveForm::begin(); ?>
        <?= $form->field($model, 'lastname')->textInput() ?>
        <?= $form->field($model, 'firstname')->textInput() ?>
        <?= $form->field($model, 'secondname')->textInput() ?>
        <?= $form->field($model, 'birthday')->textInput()->widget(DatePicker::className(), array(
            'language' => 'ru',
            'dateFormat' => 'yyyy-MM-dd',
            )
        ); ?>
        <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'phonenumber')->textInput()-> widget(MaskedInput::className(), [
            'mask' => '+7 (999) 999 99 99']); ?>
        <?= $form->field($model, 'organizationkey')->dropDownList($items,$params) ?>
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-primary']) ?>
        </div>
 
        <?php ActiveForm::end(); ?>
 
    </div>
 
</div>
