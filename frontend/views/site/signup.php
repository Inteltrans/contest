<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\jui\DatePicker;

$this->title = 'Регистрация';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-signup">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Пожалуйста, заполните следующие поля для регистрации:</p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

                <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

                <?= $form->field($model, 'email') ?>

                <?= $form->field($model, 'password')->passwordInput() ?>

                <?= $form->field($model, 'firstname')?>
                <?= $form->field($model, 'secondname')?>
                <?= $form->field($model, 'lastname')?>
                <?=$form->field($model, 'birthday')->textInput()->widget(DatePicker::className(), array(
                    'language' => 'ru',
                    'dateFormat' => 'yyyy-MM-dd',));?>
                <?php
                    $organizations = \common\models\Organizations::find()->all();
                    $items = \yii\helpers\ArrayHelper::map($organizations,'organizationkey','organizationname');
                ?>
                <?= $form->field($model, 'organization')->dropDownList($items)?>

                <div class="form-group">
                    <?= Html::submitButton('Регистрация', ['class' => 'btn btn-primary', 'name' => 'signup-button'])
                    ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
