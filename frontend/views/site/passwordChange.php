<?php
 
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

 
 
$this->params['breadcrumbs'][] = ['label'=>'Основное', 'url'=> Yii::$app->urlManager->createUrl(['site/personalarea'])];
$this->params['breadcrumbs'][] = ['label'=>'Настройки'];
   
?>
<div class="user-profile-password-change">
 
    <h1><?= Html::encode($this->title) ?></h1>
 
    <div class="user-form">
 
        <?php $form = ActiveForm::begin(); ?>
        <?= $form->field($model, 'currentPassword')->passwordInput(['maxlength' => true])->label('Существующий пароль') ?>
        <?= $form->field($model, 'newPassword')->passwordInput(['maxlength' => true])->label('Новый пароль') ?>
        <?= $form->field($model, 'newPasswordRepeat')->passwordInput(['maxlength' => true])->label('Повторите новый пароль') ?>
 
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-primary']) ?>
        </div>
 
        <?php ActiveForm::end(); ?>
 
    </div>
 
</div>