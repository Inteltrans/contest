<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;




$this->params['breadcrumbs'][] = ['label'=>'Основное'];
$this->params['breadcrumbs'][] = ['label'=>'Настройки', 'url'=> Yii::$app->urlManager->createUrl(['site/password-change'])];
   


?>

<p>
        <?= Html::a(Yii::t('app', 'Редактировать'), ['update'], ['class' => 'btn btn-primary']) ?>
    </p>

    <div class="user-profile">
 
    <h1><?= Html::encode($this->title) ?></h1>
 
    <?php
    switch ($user->role)
    {
        case \common\models\User::ROLE_USER: $role ='Пользователь'; break;
        case \common\models\User::ROLE_MODER: $role ='Проверяющий'; break;
        case \common\models\User::ROLE_ADMIN: $role ='Администратор'; break;
        default: $role = 'Роль не определена'; break;
    }

    echo DetailView::widget([
        'model' => $user,
        'attributes' => [
            'lastname',
            'firstname',
            'secondname',
            'birthday',
            [
                'attribute' => 'role',
                'value' => $role
            ],
            'email',
            'phonenumber',
            [
              'label'=>'Организация',
              'value'=>$organization,
            ],
        ],
    ]) ?>
 
</div>



