<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models;

/* @var $this yii\web\View */
/* @var $model common\models\ */

$this->title = $model->ajudicatorkey;
$this->params['breadcrumbs'][] = ['label' => 'Проверяющие', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ajudicators-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Обновить', ['update', 'id' => $model->ajudicatorkey], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->ajudicatorkey], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно хотите удалить данного проверяющего?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                     'attribute' => 'contestkey',
                     'value' => $model->contestkey0->contestname,
            ],
            [
                     'attribute' => 'userkey',
                     'value' => $model->userkey0->lastname,
            ],
        ],
    ]) ?>

</div>
