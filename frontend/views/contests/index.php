<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ContestsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Конкурсы';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="contests-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <p>

        <?php
		if(Yii::$app->user->isGuest)
			echo "Необходимо зарегистрироваться.";
		else
		{
            $userId = Yii::$app->user->identity->getId();
            $sql = "SELECT role FROM user WHERE $userId=user.id;";
            $command = Yii::$app->db->createCommand($sql);
            $role = $command->queryScalar();

            if (Yii::$app->user->can('actionContest', ['post' => $role]))
                echo Html::a('Создать конкурс', ['create'], ['class' => 'btn btn-success']);
		}
        ?>
    </p>
    <p>
                <?php
		if(!Yii::$app->user->isGuest)
			echo Html::a('Подать заявку',['requests/create'], ['class' => 'btn btn-success'])
			?>
    </p>
            <?php
		if(!Yii::$app->user->isGuest)
		{
			echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'summary'=>'',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'contestname',
            'condition',
            'datestart',
            'datefinish',
            ['class' => 'common\components\grid\ContestActionColumn'],
            
        ],
         'summary' => "",
		]);
		}
		?>
</div>
