<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Contests */

$this->title = 'Обновить Конкурс: ' . $model->contestname;
$this->params['breadcrumbs'][] = ['label' => 'Конкурсы', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->contestkey, 'url' => ['view', 'id' => $model->contestkey]];
$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="contests-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= Html::a('Скачать положение', ['download', 'id' => $model->contestkey], ['class' => 'btn btn-primary']) ?>
    
    <?= $this->render('_form', [
        'model' => $model,
        'items' => $items,
        'params'=>$params,
    ]) ?>

</div>
