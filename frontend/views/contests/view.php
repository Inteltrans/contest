<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Contests */

$this->title = $model->contestname;
$this->params['breadcrumbs'][] = ['label' => 'Конкурсы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contests-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?php
            $userId = Yii::$app->user->identity->getId();
            $sql = "SELECT role FROM user WHERE $userId=user.id;";
            $command = Yii::$app->db->createCommand($sql);
            $role = $command->queryScalar();
            $canEditCreate = Yii::$app->user->can('actionContest', ['post' => $role]);
            if ($canEditCreate) {
                echo Html::a('Обновить', ['update', 'id' => $model->contestkey,], ['class' => 'btn btn-primary']);
                echo Html::a('Удалить', ['delete', 'id' => $model->contestkey], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Вы действительно хотите удалить данный конкурс?',
                        'method' => 'post',
                    ],
                ]);
            }
        ?>
        <?= Html::a('Скачать положение', ['download', 'id' => $model->contestkey], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [ 'attribute' => 'contestname',],
            ['attribute' => 'condition',],
            ['attribute' =>'datestart',],
            ['attribute' =>'datefinish',],
            ['attribute' =>'lowage',],
            ['attribute' =>'hightage',],
            [
                     'attribute' => 'contesttypekey',
                     'value' => $model->contesttypekey0->name,
            ],
        ],
        
    ]) ?>

</div>
