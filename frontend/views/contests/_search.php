<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ContestsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="contests-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'contestkey') ?>

    <?= $form->field($model, 'contestname') ?>

    <?= $form->field($model, 'condition') ?>

    <?= $form->field($model, 'datestart') ?>

    <?= $form->field($model, 'datefinish') ?>

    <?php // echo $form->field($model, 'lowage') ?>

    <?php // echo $form->field($model, 'hightage') ?>

    <?php // echo $form->field($model, 'contesttypekey') ?>

    <div class="form-group">
        <?= Html::submitButton('Поиск', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Сброс', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
