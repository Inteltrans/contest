<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ContestUsersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Участники';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contest-users-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать участника', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'firstname',
            'secondname',
            'lastname',
            'birthday',
            // 'username',
            // 'password',
            // 'role',
            // 'phonenumber',
            // 'email:email',
            // 'organizationkey',

            ['class' => 'yii\grid\ActionColumn'],
        ],
         'summary' => "",
    ]); ?>
</div>
