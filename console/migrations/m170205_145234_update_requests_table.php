<?php

use yii\db\Migration;

class m170205_145234_update_requests_table extends Migration
{
    public function up()
    {
        $this->truncateTable('requests');

        $this->dropForeignKey('fk_request_teacher', 'requests');
        $this->dropForeignKey('fk_request_user', 'requests');
        $this->dropColumn('requests', 'userkey');
        $this->dropColumn('requests', 'teacherkey');

        $this->addColumn('requests', 'user','VARCHAR(100) NOT NULL');
        $this->addColumn('requests', 'teacher','VARCHAR(100) NOT NULL');
        $this->addColumn('requests', 'organizationkey','int(11) NOT NULL');

        $this->addForeignKey('fk_requests_organization', '{{%requests}}', 'organizationkey', '{{%organizations}}', 'organizationkey', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->truncateTable('requests');

        $this->dropForeignKey('fk_requests_organization', 'requests');
        $this->dropColumn('requests', 'user');
        $this->dropColumn('requests', 'teacher');
        $this->dropColumn('requests', 'organizationkey');

        $this->addColumn('requests', 'userkey','int(11) NOT NULL');
        $this->addColumn('requests', 'teacherkey','int(11) NOT NULL');
        $this->addForeignKey('fk_request_user', '{{%requests}}', 'userkey', '{{%user}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_request_teacher', '{{%requests}}', 'teacherkey', '{{%user}}', 'id', 'CASCADE', 'CASCADE');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
