<?php

use yii\db\Migration;

class m170205_181128_add_request_date extends Migration
{
    public function up()
    {
        $this->addColumn('requests', 'request_date', 'timestamp DEFAULT CURRENT_TIMESTAMP');

    }

    public function down()
    {
        $this->dropColumn('requests', 'request_date');

    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
