<?php

use yii\db\Migration;

class m170205_174628_add_request_owner_to_requests extends Migration
{
    public function up()
    {
        $this->addColumn('requests', 'user_owner_key','int(11) NOT NULL');

        $this->addForeignKey('fk_request_user', '{{%requests}}', 'user_owner_key', '{{%user}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropColumn('requests', 'user_owner_key');

        $this->dropForeignKey('fk_request_user', 'requests');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
