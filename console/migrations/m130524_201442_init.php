<?php

use yii\db\Migration;

class m130524_201442_init extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull()->unique(),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'email' => $this->string()->notNull()->unique(),

            'status' => $this->smallInteger()->notNull()->defaultValue(10),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            
            

            'firstname' => 'VARCHAR(30)',
            'secondname' => 'VARCHAR(30)',
            'lastname' => 'VARCHAR(30)',
            'birthday' => 'DATE',

            'role' => 'int(1)',
            'phonenumber' => 'VARCHAR(30)',

            'organizationkey' => 'int(11)',
        ], $tableOptions);
        
         $this->createTable('{{%ajudicators}}', [
            'ajudicatorkey' => 'pk',
            'contestkey' => 'int(11) NOT NULL',
            'userkey' => 'int(11) NOT NULL',
                ], $tableOptions);




        $this->createTable('{{%contest_types}}', [
            'contesttypekey' => 'pk',
            'name' => 'VARCHAR(20) NOT NULL',
            'comment' => 'VARCHAR(200)',
                ], $tableOptions);

        $this->createTable('{{%organizations}}', [
            'organizationkey' => 'pk',
            'organizationname' => 'VARCHAR(50) NOT NULL',
            'location' => 'VARCHAR(100)',
            'phonenumber' => 'VARCHAR(100)',
            'email' => 'VARCHAR(100)',
                ], $tableOptions);

        $this->createTable('{{%requests}}', [
            'requestkey' => 'pk',
            'worklink' => 'VARCHAR(100) NOT NULL',
            'contestkey' => 'int(11) NOT NULL',
            'userkey' => 'int(11) NOT NULL',
            'teacherkey' => 'int(11)',
                ], $tableOptions);


        $this->createTable('{{%results}}', [
            'resultkey' => 'pk',
            'mark' => 'int(11) NOT NULL',
            'ajudicatorkey' => 'int(11) NOT NULL',
            'requestkey' => 'int(11) NOT NULL',
                ], $tableOptions);

        
        $this->createTable('{{%contests}}', [
             'contestkey' => 'pk',
            'contestname' => 'VARCHAR(30) NOT NULL',
            'condition' => 'VARCHAR(100)NOT NULL',
            'datestart' => 'DATE NOT NULL',
            'datefinish' => 'DATE NOT NULL',
            'lowage' => 'int(2) NOT NULL',
            'hightage' => 'int(2) NOT NULL',
            'contesttypekey' => 'int(11) NOT NULL',
                ], $tableOptions);
        


        $this->addForeignKey('fk_results_ajudicators', '{{%results}}', 'ajudicatorkey', '{{%ajudicators}}', 'ajudicatorkey', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_results_requests', '{{%results}}', 'requestkey', '{{%requests}}', 'requestkey', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_ajudicators_contest', '{{%ajudicators}}', 'contestkey', '{{%contests}}', 'contestkey', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_ajudicators_users', '{{%ajudicators}}', 'userkey', '{{%user}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_contest_contest_type', '{{%contests}}', 'contesttypekey', '{{%contest_types}}', 'contesttypekey', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_requests_contest', '{{%requests}}', 'contestkey', '{{%contests}}', 'contestkey', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_request_user', '{{%requests}}', 'userkey', '{{%user}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_request_teacher', '{{%requests}}', 'teacherkey', '{{%user}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_user_organization', '{{%user}}', 'organizationkey', '{{%organizations}}', 'organizationkey', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        echo "mirgation m130524_201442_init cannot be reverted.\n";

        return false;
    }
}
