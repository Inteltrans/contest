<?php
namespace console\controllers;
use common\components\rbac\ActionContestRule;
use common\components\rbac\ActionResultsRule;
use common\components\rbac\UserOwnerRule;
use Yii;
use yii\console\Controller;
use common\components\rbac\UserRoleRule;
class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;
        $auth->removeAll(); //удаляем старые данные
        //Создадим для примера права для доступа к админке
        $dashboard = $auth->createPermission('dashboard');
        $dashboard->description = 'Админ панель';
        $auth->add($dashboard);
        //Включаем наш обработчик
        $rule = new UserRoleRule();
        $auth->add($rule);
        //Добавляем роли
        $user = $auth->createRole('user');
        $user->description = 'Пользователь';
        $user->ruleName = $rule->name;
        $auth->add($user);
        $moder = $auth->createRole('moder');
        $moder->description = 'Модератор';
        $moder->ruleName = $rule->name;
        $auth->add($moder);
        //Добавляем потомков
        $auth->addChild($moder, $user);
        $auth->addChild($moder, $dashboard);
        $admin = $auth->createRole('admin');
        $admin->description = 'Администратор';
        $admin->ruleName = $rule->name;
        $auth->add($admin);
        $auth->addChild($admin, $moder);


        $ownerRule = new UserOwnerRule();
        $auth->add($ownerRule);
        $updatePerm = $auth->createPermission('updateOwn');
        $updatePerm->ruleName=$ownerRule->name;
        $auth->add($updatePerm);

        $auth->addChild($user,$updatePerm);
        $auth->addChild($moder,$updatePerm);
        $auth->addChild($admin,$updatePerm);

        $actionContestRule = new ActionContestRule();
        $auth->add($actionContestRule);
        $contestPerm = $auth->createPermission('actionContest');
        $contestPerm->ruleName=$actionContestRule->name;
        $auth->add($contestPerm);

        $auth->addChild($user,$contestPerm);
        $auth->addChild($moder,$contestPerm);
        $auth->addChild($admin,$contestPerm);

        $actionResultsRule = new ActionResultsRule();
        $auth->add($actionResultsRule);
        $resultsPerm = $auth->createPermission('actionResult');
        $resultsPerm->ruleName=$actionResultsRule->name;
        $auth->add($resultsPerm);

        $auth->addChild($user,$resultsPerm);
        $auth->addChild($moder,$resultsPerm);
        $auth->addChild($admin,$resultsPerm);
    }
}